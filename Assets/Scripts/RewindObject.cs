﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        pointsInTime = new List<PointInTime>();
    }

    public float RecordTime = 5f;

    List<PointInTime> pointsInTime;

    Rigidbody2D rigid;

    bool isRewinding;

    PointInTime lastPoint;


    // Update is called once per frame
    void Update()
    {

        if (isRewinding != RewindController.IsRewinding)
        {

            if (RewindController.IsRewinding)
            {
                StartRewind();
            }
            else
            {
                StopRewind();
            }
        }

    }

    private void FixedUpdate()
    {
        if (isRewinding)
        {
            Rewind();
        }
        else
        {
            Record();
        }
    }

    private void Rewind()
    {
        if (pointsInTime.Count > 0)
        {
            PointInTime pointInTime = pointsInTime [0];
            transform.rotation = pointInTime.Rotation;
            transform.position = pointInTime.Position;

            lastPoint = pointInTime;

            pointsInTime.RemoveAt(0);
        }
    }

    void Record()
    {
        if (pointsInTime.Count > Mathf.Round(5f / Time.fixedUnscaledDeltaTime))
        {
            pointsInTime.RemoveAt(pointsInTime.Count - 1);
        }
        pointsInTime.Insert(0, new PointInTime(transform.position, transform.rotation, rigid.velocity, rigid.angularVelocity));

    }

    void StartRewind()
    {
        isRewinding = true;
        rigid.isKinematic = true;

        rigid.velocity = Vector2.zero;
        rigid.angularVelocity = 0;
    }
    void StopRewind()
    {
        isRewinding = false;
        rigid.isKinematic = false;

        rigid.velocity = lastPoint.Velocity;
        rigid.angularVelocity = lastPoint.AngularVelocity;
    }


    private class PointInTime
    {

        public PointInTime(Vector2 Position, Quaternion Rotation, Vector2 Velocity, float AngularVelocity)
        {
            this.Position = Position;
            this.Rotation = Rotation;
            this.Velocity = Velocity;
            this.AngularVelocity = AngularVelocity;
        }

        public Vector2 Position;
        public Quaternion Rotation;
        public Vector2 Velocity;
        public float AngularVelocity;

    }


}
