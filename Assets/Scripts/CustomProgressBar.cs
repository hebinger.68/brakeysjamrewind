﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomProgressBar : MonoBehaviour
{

    public Image bar;

    // Start is called before the first frame update


    public void SetPercentage(float percentage)
    {
        bar.fillAmount = percentage;
    }

}
