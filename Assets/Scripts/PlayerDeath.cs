﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{


    private void Start()
    {
        rigid = this.GetComponent<Rigidbody2D>();

        animator = this.GetComponentInChildren<Animator>();
        player = this.GetComponent<PlayerController>();
    }


    public LayerMask EnemyLayer;

    private Animator animator;

    private Rigidbody2D rigid;

    private PlayerController player;


    public Canvas gameCanvas;
    public Canvas deathCanvas;
    public TextMeshProUGUI text;
    public Timer timer;


    public Sound DeathSound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!player.IsAlive) return;


        if (((1 << collision.gameObject.layer) & EnemyLayer.value) != 0)
        {
            player.IsAlive = false;
            SetAnimation("Player_Death", false);
            rigid.simulated = false;

            timer.IsPaused = true;

            StartCoroutine(DeathUI());

            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(DeathSound, DeathSound.clip.name);

        }
    }

    private void SetAnimation(string animation, bool repeat)
    {
        //prevent animation glitches
        if (animation.Equals(animator.GetCurrentAnimatorClipInfo(0) [0].clip.name) && !repeat) return;
        Debug.Log("Type: " + animation);
        animator.Play(animation);
    }

    IEnumerator DeathUI()
    {

        yield return new WaitForSeconds(2);

        gameCanvas.gameObject.SetActive(false);
        deathCanvas.gameObject.SetActive(true);
        text.text = timer.ToString();


    }




}
