﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetLevel : MonoBehaviour
{


    public void ResetButton()
    {
        Reset();
    }

    bool isReseting = false;

    public void Reset()
    {
        if (isReseting) return;
        isReseting = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reset();
        }
    }

}
