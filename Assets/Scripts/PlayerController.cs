﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{

    public bool IsAlive = true;

    public float Speed = 5;
    public float JumpVelocity = 1;

    public LayerMask GroundLayer;
    public float GroundDistance = 0.1f;
    public float GroundWidth = 0.5f;

    private Rigidbody2D rigid;

    private Animator animator;

    private SpriteRenderer sprite;


    public Vector3 GroundPosition;

    float GroundedRemember = 0;
    float JumpPressedRemember = 0;
    public float JumpPressedRememberTime = 0.2f;
    public float GroundedRememberTime = 0.25f;

    [Range(0, 1)]
    public float CutJumpHeight = 0.5f;

    public Direction direction = Direction.Right;

    public enum Direction
    {
        Left,
        Right
    }


    [Space]
    public bool DrawGizmos;

    // Start is called before the first frame update
    private void Start()
    {
        rigid = this.GetComponent<Rigidbody2D>();

        animator = this.GetComponentInChildren<Animator>();
        sprite = this.GetComponentInChildren<SpriteRenderer>();
    }

    bool wasGounded = true;

    private void Update()
    {
        if (IsAlive)
        {


            float x = Input.GetAxis("Horizontal") * Speed;


            rigid.velocity = new Vector2(x, rigid.velocity.y);

            bool Grounded = IsGrounded();
            if (Grounded != wasGounded && Grounded == true)
            {
                AudioManager.AUDIO_MANAGER.PlayAudioGlobal(jumpSoundLand, jumpSoundLand.clip.name);
            }
            wasGounded = Grounded;

            GroundedRemember -= Time.unscaledDeltaTime;
            if (Grounded)
            {
                GroundedRemember = GroundedRememberTime;
            }

            JumpPressedRemember -= Time.unscaledDeltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                JumpPressedRemember = JumpPressedRememberTime;
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                if (rigid.velocity.y > 0)
                {
                    rigid.velocity = new Vector2(rigid.velocity.x, rigid.velocity.y * CutJumpHeight);
                }
            }

            if ((JumpPressedRemember > 0) && (GroundedRemember > 0))
            {
                JumpPressedRemember = 0;
                GroundedRemember = 0;
                rigid.velocity = new Vector2(rigid.velocity.x, JumpVelocity);
                AudioManager.AUDIO_MANAGER.PlayAudioGlobal(jumpSound, jumpSound.clip.name);
            }

        }



        AnimationUpdate();

    }


    private string AnimatorStatusWalking = "Walking";

    private void AnimationUpdate()
    {

        float x = Input.GetAxisRaw("Horizontal");

        if (x > 0)
        {
            direction = Direction.Right;
        }
        else if (x < 0)
        {
            direction = Direction.Left;
        }

        animator.SetBool(AnimatorStatusWalking, (Input.GetAxisRaw("Horizontal") != 0));
        sprite.flipX = (direction == Direction.Left);

    }

    private bool IsGrounded()
    {
        for (int i = -1; i < 2; i++)
        {
            if (Physics2D.Raycast((Vector3.left * i * GroundWidth) + GroundPosition + transform.position, Vector2.down, GroundDistance, GroundLayer))
            {
                return true;
            }
        }

        return false;

    }

    private void OnDrawGizmos()
    {

        if (!DrawGizmos) return;

        //Gizmos.DrawWireSphere(GroundPosition + transform.position, 0.05f);

        Gizmos.color = Color.red;

        for (int i = -1; i < 2; i++)
        {
            Gizmos.DrawLine((Vector3.left * i * GroundWidth) + GroundPosition + transform.position, (Vector3.left * i * GroundWidth) + GroundPosition + transform.position + Vector3.down * GroundDistance);
        }
    }

    public Sound jumpSound;
    public Sound jumpSoundLand;
    public Sound walkSound;

}
