﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewindController : MonoBehaviour
{

    public static bool IsRewinding = false;

    public CustomProgressBar bar;

    public SpriteRenderer barRender;
    public SpriteRenderer houreGlassRender;
    public SpriteRenderer houreGlassBackRender;
    public Image barFillRender;


    public Material barColor;
    public Material barColorRewinding;
    public Material barColorEmpty;

    public PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        IsRewinding = false;

        Time.fixedDeltaTime = 0.02f / 2;
    }

    float timeRewinding = 0;

    public float RewindTimeWindow = 5;
    public float RewindTimeWindowMinStart = 2;

    bool lastRewindCommand = false;

    // Update is called once per frame
    void Update()
    {
        if (!player.IsAlive) return;

        if (Input.GetKey(KeyCode.E) && ((lastRewindCommand && timeRewinding < RewindTimeWindow)
            || (timeRewinding < (RewindTimeWindow - RewindTimeWindowMinStart) && Input.GetKeyDown(KeyCode.E))))
        {

            StartRewind();
            lastRewindCommand = true;

        }
        else if (timeRewinding > 0)
        {

            StopRewind();
            lastRewindCommand = false;
        }

        if (IsRewinding)
        {
            timeRewinding += Time.deltaTime;
        }
        else if (timeRewinding > 0)
        {
            timeRewinding -= Time.deltaTime;
        }



        UpdateBarAnimation();

    }

    private void UpdateBarAnimation()
    {
        bar.SetPercentage(1 - timeRewinding / RewindTimeWindow);

        if (timeRewinding > (RewindTimeWindow - RewindTimeWindowMinStart) && !IsRewinding)
        {
            barRender.material = barColorEmpty;
            barFillRender.material = barColorEmpty;
            houreGlassRender.material = barColorEmpty;
            houreGlassBackRender.material = barColorEmpty;
        }
        else
        if (IsRewinding)
        {
            barRender.material = barColorRewinding;
            barFillRender.material = barColorRewinding;
            houreGlassRender.material = barColorRewinding;
            houreGlassBackRender.material = barColorRewinding;

        }
        else
        {
            barRender.material = barColor;
            barFillRender.material = barColor;
            houreGlassRender.material = barColor;
            houreGlassBackRender.material = barColor;
        }

    }

    public Sound soundStartRewind;
    public Sound soundStopRewind;

    private void StartRewind()
    {

        if (!IsRewinding)
        {
            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(soundStartRewind, soundStartRewind.clip.name);
        }

        IsRewinding = true;
        Time.timeScale = 0.7f;

    }

    private void StopRewind()
    {
        if (IsRewinding)
        {
            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(soundStopRewind, soundStopRewind.clip.name);
        }

        IsRewinding = false;
        Time.timeScale = 1f;
    }



}
