﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DustAI : MonoBehaviour
{

    public Transform target;
    private Rigidbody2D rigid;

    public float Speed = 1;

    // Start is called before the first frame update
    void Start()
    {
        target = FindObjectOfType<PlayerController>().gameObject.transform;
        rigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = target.position - transform.position;
        direction.z = 0;
        direction.Normalize();

        rigid.velocity = direction * Speed;

    }
}
