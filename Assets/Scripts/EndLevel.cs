﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour
{
    // Start is called before the first frame update
    public PlayerController Player;

    public Timer timer;

    public Animation EndAnim;

    public TextMeshPro test;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        try
        {

            if (collision.gameObject == Player.gameObject)
            {

                //EndAnimation.SetActive(true);

                Player.GetComponent<Rigidbody2D>().simulated = false;

                timer.IsPaused = true;

                EndAnim.gameObject.SetActive(true);

                test.gameObject.SetActive(true);

                if (PlayerPrefs.HasKey("highscore_level" + SceneManager.GetActiveScene().buildIndex))
                {
                    float highscore = PlayerPrefs.GetFloat("highscore_level" + SceneManager.GetActiveScene().buildIndex);


                    if (highscore > timer.time)
                    {
                        test.text = "New Highscore\n" + timer.ToString();
                        PlayerPrefs.SetFloat("highscore_level" + SceneManager.GetActiveScene().buildIndex, timer.time);
                    }
                    else
                    {
                        test.text = "Highscore\n" + Timer.TimeToString(highscore);
                    }
                }
                else
                {
                    test.text = "New Highscore\n" + timer.ToString();
                    PlayerPrefs.SetFloat("highscore_level" + SceneManager.GetActiveScene().buildIndex, timer.time);
                }


                StartCoroutine(Delay());

                AudioManager.AUDIO_MANAGER.PlayAudioGlobal(EndSound, EndSound.clip.name);

            }

        }
        catch { }
    }

    public Sound EndSound;



    IEnumerator Delay()
    {
        yield return new WaitForSeconds(3);

        change = true;
    }

    bool change = false;

    private void Update()
    {
        if (change)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            change = false;
        }
    }

}
