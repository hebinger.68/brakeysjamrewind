﻿using UnityEngine;

public class OmnicientObject : MonoBehaviour
{
    private static OmnicientObject instance = null;
    public static OmnicientObject Instance
    {
        get { return instance; }
    }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
}