﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveAtStart : MonoBehaviour
{

    public bool Active = true;

    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(Active);
    }
}
