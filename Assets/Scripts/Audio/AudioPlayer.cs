﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

class AudioPlayer : MonoBehaviour {

    public AudioSource source;

    public bool isPaused = true;

    private void Awake() {
        source = gameObject.AddComponent<AudioSource>();
    }

    public void PlaySound(Sound s) {
        source.clip = s.clip;
        source.volume = s.volume;
        source.pitch = s.pitch;
        source.loop = s.loop;
        source.spatialBlend = s.spacialBlend;
        source.Play();
        isPaused = false;
    }

    private void Update() {
        if (isPaused) return;
        if (!source.isPlaying) {
            Destroy(this.gameObject);
        }
    }





}

