﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class DistanceManager : MonoBehaviour
{

    public List<GameObject> gameObjects = new List<GameObject>();

    public Transform Player;

    public float ActiveDistance = 5;


    // Update is called once per frame
    void Update()
    {
        foreach (GameObject objects in gameObjects)
        {

            //Debug.Log(Vector2.Distance(objects.transform.position, Player.position));

            if (Vector2.Distance(objects.transform.position, Player.position) < ActiveDistance)
            {
                objects.SetActive(true);
            }
            else
            {
                objects.SetActive(false);
            }

        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(Player.position, ActiveDistance);
    }
}
